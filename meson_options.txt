option('logpath', type: 'string', value: '/tmp',
	description: 'Base path for server log and config files (default: /tmp)')
