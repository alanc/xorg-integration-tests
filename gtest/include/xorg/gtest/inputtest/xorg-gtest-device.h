/*******************************************************************************
 *
 * X testing environment - Google Test environment feat. dummy x server
 *
 * Copyright (C) 2020 Povilas Kanapickas <povilas@radix.lt>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 ******************************************************************************/

#ifndef XORG_INPUTTEST_DEVICE_H_
#define XORG_INPUTTEST_DEVICE_H_

#include <memory>
#include <string>
#include "xorg-gtest-valuators.h"

namespace xorg {
namespace testing {
namespace inputtest {

enum class DeviceType {
    KEYBOARD,
    POINTER,
    POINTER_ABSOLUTE,
    POINTER_GESTURE,
    POINTER_ABSOLUTE_PROXIMITY,
    TOUCH,
};

#define XORG_INPUTTEST_DRIVER "inputtest"

/**
 * A device to replay events through the xf86-input-inputtest input driver
 */
class Device {
public:
    /**
     * Create a new device context.
     *
     * @param [in] socket_path Path where the UNIX domain socket used for communication with the
     *             driver will be constructed.
     * @param [in] type The type of the device which defines what operations are available.
     */
    explicit Device(const std::string& socket_path, DeviceType type);
    ~Device();

    /**
     * Returns the options that the server needs to be configured for this device.
     */
    std::string GetOptions() const;

    /**
     * Waits for the connection to the driver being established.
     * Throws an exception if the files are not available after 10 seconds.
     */
    void WaitForDriver();

    /**
     * All of the functions below result in an input event being sent to the X server.
     * WaitForDriver() must be called before using them.
     */
    void RelMotion(double x, double y);
    void RelMotion(double x, double y, const Valuators& extra_valuators);

    void AbsMotion(double x, double y);
    void AbsMotion(double x, double y, const Valuators& extra_valuators);

    void ProximityIn(const Valuators& valuators);
    void ProximityOut(const Valuators& valuators);

    void ButtonDown(std::int32_t button);
    void ButtonUp(std::int32_t button);

    void KeyDown(std::int32_t key_code);
    void KeyUp(std::int32_t key_code);

    void TouchBegin(double x, double y, std::uint32_t id);
    void TouchBegin(double x, double y, std::uint32_t id, const Valuators& extra_valuators);
    void TouchUpdate(double x, double y, std::uint32_t id);
    void TouchUpdate(double x, double y, std::uint32_t id, const Valuators& extra_valuators);
    void TouchEnd(double x, double y, std::uint32_t id);
    void TouchEnd(double x, double y, std::uint32_t id, const Valuators& extra_valuators);

    void GestureSwipeBegin(std::uint16_t num_touches, double delta_x, double delta_y,
                           double delta_unaccel_x, double delta_unaccel_y);
    void GestureSwipeUpdate(std::uint16_t num_touches, double delta_x, double delta_y,
                            double delta_unaccel_x, double delta_unaccel_y);
    void GestureSwipeEnd(std::uint16_t num_touches, double delta_x, double delta_y,
                         double delta_unaccel_x, double delta_unaccel_y);
    void GestureSwipeCancel(std::uint16_t num_touches, double delta_x, double delta_y,
                            double delta_unaccel_x, double delta_unaccel_y);

    void GesturePinchBegin(std::uint16_t num_touches, double delta_x, double delta_y,
                           double delta_unaccel_x, double delta_unaccel_y,
                           double scale, double delta_angle);
    void GesturePinchUpdate(std::uint16_t num_touches, double delta_x, double delta_y,
                            double delta_unaccel_x, double delta_unaccel_y,
                            double scale, double delta_angle);
    void GesturePinchEnd(std::uint16_t num_touches, double delta_x, double delta_y,
                         double delta_unaccel_x, double delta_unaccel_y,
                         double scale, double delta_angle);
    void GesturePinchCancel(std::uint16_t num_touches, double delta_x, double delta_y,
                            double delta_unaccel_x, double delta_unaccel_y,
                            double scale, double delta_angle);
private:

    struct Private;
    std::unique_ptr<Private> d_;
};

} // namespace inputtest
} // namespace testing
} // namespace xorg

#endif // XORG_INPUTTEST_DEVICE_H_
